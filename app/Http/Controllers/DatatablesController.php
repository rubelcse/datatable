<?php

namespace App\Http\Controllers;

use App\User;
Use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DatatablesController extends Controller
{
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        return view('datatables.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        //dd(request()->all());
        $data=User::all();
        return Datatables::of(User::query($data))->make(true);
    }

    public function update(Request $request)
    {
        //$user=User::findOrFail($request->currentId);
        //dump($request->all());
        $data['order']=$request->toPosition;
        User::whereId($request->currentId)->update($data);
        //User::update($data)->where('id',$request->currentId);
        //return Datatables::of(User::query($query))->make(true);
        return redirect()->route('datatables');
    }
}
