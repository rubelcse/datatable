@extends('layouts.master')

@section('content')
  <table class="table table-bordered" id="example">
        <thead>
        <tr>
            <th>Order</th>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.fn.dataTable.defaults.rowReorder = true;
    $(document).ready(function(){
        var table = $('#example').DataTable({
            'ajax': {
                'url': '{!! route('datatables.data') !!}'
            },
            'createdRow': function(row, data, dataIndex){
                $(row).attr('order', 'row-' + dataIndex);
            },columns: [
                { data: 'order', name: 'order', searchable:true, orderable: true},
                { data: 'id', name: 'id'},
                { data: 'name', name: 'name', searchable:true  },
                { data: 'email', name: 'email', searchable:true },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at',orderable: true }
            ],
            "order": [[ 0,4, "desc" ]],
            processing: true,
        }).columns([1]).visible(false).rowReordering();
    });
</script>
@endpush
